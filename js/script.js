/* Опишіть своїми словами, що таке метод об'єкту.
Це функція в середині об'єкта, яка взаємодіє із змінними в середині об'єкта та аргументами,
що передаються.

Який тип даних може мати значення властивості об'єкта?.
Number, String, Boolean, null, інший об'єкт.

Об'єкт це посилальний тип даних. Що означає це поняття?
Те, що змінна фактично зберігає посилання на об'єкт, а не сам об'єкт. При копіюванні
змінної копіюється посилання, а не сам об'єкт. Тобто дві змінні ведуть до одного 
і того самого об'єкту. 

*/

function createNewUser(a = prompt('What is your Name?'), b = prompt('What is your Last name?')) {
    const newUser = {
        firstName: a,
        lastName: b,
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    }
    return newUser;
}

let user1 = createNewUser();

console.log(user1.getLogin());











